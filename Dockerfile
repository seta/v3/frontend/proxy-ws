FROM nginx:1.27.3

ARG PROJECT_CONF=project

RUN rm /etc/nginx/nginx.conf
COPY nginx.conf /etc/nginx/

RUN mkdir /etc/nginx/templates
COPY ${PROJECT_CONF}.conf /etc/nginx/templates/default.conf.template
