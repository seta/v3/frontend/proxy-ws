# proxy-ws

## Container Registry
If you are not already logged in, you need to authenticate to the Container Registry by using your GitLab username and password. 
If you have Two-Factor Authentication enabled, use a [Personal Access Token](https://code.europa.eu/help/user/profile/personal_access_tokens) instead of a password.

Login with Personal Access Token:
```
docker login code.europa.eu:4567
```

### Build Image for Production 
Build production image from the root of the project:
```
docker build -t code.europa.eu:4567/seta/v3/frontend/proxy-ws:{$VERSION} .
```

Push to remote container:
```
docker push code.europa.eu:4567/seta/v3/frontend/proxy-ws:{$VERSION}
```

### Build Image for Development 

Build development image from the root of the project:
```
docker build -t code.europa.eu:4567/seta/v3/frontend/proxy-ws:dev --build-arg="PROJECT_CONF=project_dev" .
```

Push to remote container:
```
docker push code.europa.eu:4567/seta/v3/frontend/proxy-ws:dev
```

### Build Image for Test 
Build test image from the root of the project:
```
docker build -t code.europa.eu:4567/seta/v3/frontend/proxy-ws:test --build-arg="PROJECT_CONF=project_test" .
```

Push to remote container:
```
docker push code.europa.eu:4567/seta/v3/frontend/proxy-ws:test
```

## Environment variables

* **WHITELIST_INGEST_IP=0.0.0.0**: Whitelisted IP of the ingest service;
* **BULK_INSERT_BODY_SIZE=1G**: Maximum file size with the chunks for bulk insert (defaults to 1 gigabyte).
